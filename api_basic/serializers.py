import imp
from rest_framework import serializers
from . models import Articles

# ######################################################################
#                      database model ---> json [de-serializer]
#                      json ---> database model [serializer]                         
# ######################################################################

# see parent class docs. here

class ArticleSerializer(serializers.ModelSerializer):
    class Meta:
        model = Articles
        # database already has an 'id' field
        fields = ('id', 'Author', 'Title', 'Email', 'Date')