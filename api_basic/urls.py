import imp
from django.contrib import admin
from django.urls import path
# from . views import article_list, get_article, ArticlesList, ArticleUpdate, 
from . views import ArticleViewset
from . models import Articles
from . serializers import ArticleSerializer
from rest_framework.routers import DefaultRouter


router = DefaultRouter()
router.register(r'articles', ArticleViewset, basename='article')
urlpatterns = router.urls

# urlpatterns = [
#     path('articles/', article_list),
#     path('get_article/<int:pk>', get_article),
#     path('list_articles/', ArticlesList.as_view(), name='articles-list'),
#     path('update/<int:pk>', ArticleUpdate.as_view(), name='article-update'),
    
# ]