from statistics import mode
from django.db import models

# ######################################################################
#                      for database                        
# ####################################################################

# Create your models here.
class Articles(models.Model):
    Author = models.CharField(max_length=100)
    Title = models.CharField(max_length=100)
    Email = models.EmailField(max_length=100)
    Date = models.DateTimeField(auto_now_add=True)
    
    
    def __str__(self):
        return self.Title+self.Email