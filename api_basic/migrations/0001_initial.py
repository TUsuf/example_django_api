# Generated by Django 3.2.12 on 2022-04-10 11:13

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Articles',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('Author', models.CharField(max_length=100)),
                ('Title', models.CharField(max_length=100)),
                ('Email', models.EmailField(max_length=100)),
                ('Date', models.DateField(auto_now_add=True)),
            ],
        ),
    ]
