import imp
from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
from rest_framework.parsers import JSONParser
from torch import view_as_complex
from yaml import serialize
from . models import Articles
from . serializers import ArticleSerializer
from rest_framework import status, generics, viewsets
from django.views.decorators.csrf import csrf_exempt
from rest_framework.permissions import IsAuthenticated
# Create your views here.
# ######################################################################
#             for the views that could be linked with endpoint url                         
# ######################################################################

# کس اینڈ پوائنٹ پہ کیا ہو گا

# @csrf_exempt
# def article_list(request):
    
#     if request.method=="GET":
#         # get all the articles from database
#         articles = Articles.objects.all()
#         articles_serialized = ArticleSerializer(articles, many=True)
#         return JsonResponse(
#             articles_serialized.data, 
#             safe=False, 
#             status=status.HTTP_200_OK)
    
#     elif request.method == 'POST':
#         # parse the json
#         parsed_json = JSONParser().parse(request)
#         # de-serialization ---> convert javascript to DB data 
#         serialized = ArticleSerializer(data=parsed_json)
#         if serialized.is_valid():
#             # if validation performed ---> save the data in database
#             serialized.save()
#             return JsonResponse(serialized.data, 
#                                 status=status.HTTP_200_OK, 
#                                 safe=False)
#         return JsonResponse(serialized.errors, status=status.HTTP_400_BAD_REQUEST)
    
# @csrf_exempt
# def get_article(request, pk):
#     try:
#         # try to get the article
#         article = Articles.objects.get(pk=pk)
#     except Articles.DoesNotExist:
#         # if article doesn't exist in DB , return exception
#         # return HttpResponse(status=status.HTTP_404_NOT_FOUND)
#         return JsonResponse({'error': 'Article does not exist'}, 
#                             status=status.HTTP_404_NOT_FOUND, 
#                             safe=False)
    
#     if request.method == 'GET':
#         article_serialized = ArticleSerializer(article)
#         return JsonResponse(article_serialized.data, 
#                             status=status.HTTP_200_OK, 
#                             safe=False)
        
#     elif request.method == 'PUT':
#         # parse the request to get new values
#         parserd_json = JSONParser().parse(request)
#         # pass in the article [selected by pk] as instance and update the data
#         ser_data = ArticleSerializer(article, data=parserd_json)
#         if ser_data.is_valid():
#             ser_data.save()
#         return JsonResponse(ser_data.data)
    
#     elif request.method == "DELETE":
#         article.delete()
#         return JsonResponse({"deleted" : "OK"}, safe=False, status=status.HTTP_200_OK)

    
# class ArticlesList(generics.ListCreateAPIView):
#     queryset = Articles.objects.all()
#     serializer_class = ArticleSerializer
    
    
# class ArticleUpdate(generics.RetrieveUpdateAPIView):
#     queryset = Articles.objects.all()
#     serializer_class = ArticleSerializer
#     lookup_field = 'pk'
    
class ArticleViewset(viewsets.ModelViewSet):
    permission_classes = [IsAuthenticated]
    queryset = Articles.objects.all()
    serializer_class = ArticleSerializer

