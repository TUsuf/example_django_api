Root 🌴 dir is the one containing the `manage.py`  file.


# For running the server

```
python manage.py runserver
```


# See docs. to test the api

```
https://docs.google.com/document/d/1LLuy3xk_fUTky7P13hRo2M96C9kg3bmOFUds1fYty7c/edit?usp=sharing
```